## marconius - geany

Ansible role for control of Geany package (via apt)


### Variables

Available variables with defaults

```yaml
geany_ppa: ppa:geany-dev/ppa    # Geany PPA for newer versions of Geany
geany_state: latest             # State of geany package
```

### License

Licensed under the MIT License. See the LICENSE file for details.

### Examples

```yaml
- hosts: localhost
  roles:
    - role: marconius.geany
      sudo: yes
```

or if you want to make sure that Geany is not installed ...

```yaml
- hosts: localhost
  roles:
    - role: marconius.geany
      sudo: yes
      geany_ppa: No
      geany_state: absent 
